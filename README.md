# README #

スイッチロール先のロールにIAMユーザーの信頼関係を追加方法

### 修正対象リソース ###

* ./module/iam/role/admin/adminRoleAssumePolicy.json
* ./module/iam/role/read_only/readOnlyRoleAssumePolicy.json

### 追加方法 ###

1. ./module/iam/role/admin/adminRoleAssumePolicy.json

```
# Principal.AWSの配列にadmin権限を与えたいユーザーのARNを追加する。
      "Principal": {
        "AWS": [
          "arn:aws:iam::842849860514:user/infra_yuki_matsumoto"
        ]
      },
```
2. ./module/iam/role/read_only/readOnlyRoleAssumePolicy.json
```
# Principal.AWSの配列にreadOnly権限を与えたいユーザーのARNを追加する。
      "Principal": {
        "AWS": [
          "arn:aws:iam::842849860514:user/infra_yuki_matsumoto"
        ]
      },
```