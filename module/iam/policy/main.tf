resource aws_iam_policy admin_iam_policy {
  name        = "DeveloperAdministratorPolicy"
  path        = "/"
  policy = file("../../module/iam/policy/policy.json")
}
