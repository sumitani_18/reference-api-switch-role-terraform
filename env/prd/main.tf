module admin_iam_policy {
  source     = "../../module/iam/policy"
}

module admin_role {
  source     = "../../module/iam/role/admin"
  name = "DeveloperAdministrator"
  iam_policy_arn = module.admin_iam_policy.policy_arn
  assume_role_policy = file("./assume_role/DeveloperAdministratorRoleAssumePolicy.json")
}

# module ip_restricted_admin_role {
#   source     = "./module/iam/role/admin"
#   name = "IPRestrictedDeveloperAdministrator"
#   iam_policy_arn = module.admin_iam_policy.policy_arn
#   assume_role_policy = file("./assume_role/IPRestrictedDeveloperAdministratorRoleAssumePolicy.json")
# }

module read_only_role {
  source     = "../../module/iam/role/read_only"
  assume_role_policy = file("./assume_role/ReadOnlyRoleAssumePolicy.json")
}
