terraform {
  required_version = ">=0.13"
  backend "s3" {
    bucket         = "terraform-state-704313567601"
    region         = "ap-northeast-1"
    key            = "reference-api-dev-switch-role/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "terraform-state-lock"
  }
}

provider aws {
  region  = "ap-northeast-1"
}