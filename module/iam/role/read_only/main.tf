resource aws_iam_role aws_read_only_role {
  name = "ReadOnly"
  assume_role_policy = var.assume_role_policy
}

resource aws_iam_role_policy_attachment aws_read_only_role_attachement {
  role       = aws_iam_role.aws_read_only_role.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource aws_iam_role_policy_attachment aws_read_only_role_supprt_access_attachement {
  role       = aws_iam_role.aws_read_only_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSSupportAccess"
}
