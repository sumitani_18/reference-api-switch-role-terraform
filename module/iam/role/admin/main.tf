resource aws_iam_role aws_admin_role {
  name = var.name
  assume_role_policy = var.assume_role_policy
}

resource aws_iam_role_policy_attachment aws_admin_role_power_user_attachement {
  role       = aws_iam_role.aws_admin_role.name
  policy_arn = "arn:aws:iam::aws:policy/PowerUserAccess"
}

resource aws_iam_role_policy_attachment aws_admin_role_iam_read_only_attachement {
  role       = aws_iam_role.aws_admin_role.name
  policy_arn = "arn:aws:iam::aws:policy/IAMReadOnlyAccess"
}

resource aws_iam_role_policy_attachment aws_admin_role_iam_attachement {
  role       = aws_iam_role.aws_admin_role.name
  policy_arn = var.iam_policy_arn
}
