terraform {
  required_version = ">=0.13"
  backend "s3" {
    bucket         = "terraform-state-bucket-078001581952"
    region         = "ap-northeast-1"
    key            = "reference-api-dev-switch-role/prd/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "terraform-state-lock-table"
  }
}

provider aws {
  region  = "ap-northeast-1"
}
